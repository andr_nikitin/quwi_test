export default function({redirect, store}) {
    if (!store.state.userToken) {
        return redirect('/login');
    }
    return Promise.resolve()
}
