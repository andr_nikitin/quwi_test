import Vuex from 'vuex';
import Vue from 'vue';
import {setAuthToken, resetAuthToken} from '~/utils/auth';
import cookies from 'js-cookie';
import cookie from 'cookie';

const createStore = () => {
  return new Vuex.Store({
    state: {
      authUser: null,
    },
    mutations: {
      set_user(store, data) {
        store.authUser = data;
        store.userToken = data.token;
      },
      set_user_token(store, data) {
        store.userToken = data;
      },
      reset_user(store) {
        store.authUser = null;
        store.userToken = null;
      },
    },
    actions: {
      nuxtServerInit({commit}, {req}) {
        return new Promise((resolve, reject) => {
          const cookiesList = cookie.parse(req.headers.cookie || '');
          if (cookiesList.hasOwnProperty('x-access-token')) {
            setAuthToken(cookiesList['x-access-token']);
            commit('set_user_token', cookiesList['x-access-token']);
            resolve(true);
          } else {
            resetAuthToken();
            resolve(false);
          }
        });
      },
      login({commit}, data) {
        return new Promise((resolve, reject) => {
          this.$axios.post('auth/login', {
            email: data.email,
            password: data.password,
          }).then(resp => {
            commit('set_user', resp.data);
            setAuthToken(resp.data.token);
            cookies.set('x-access-token', resp.data.token, {expires: 7});
            resolve(true)
          }).catch(err => {
            commit('reset_user');
            reject(err);
          });
        })
      },
    },
    getters: {
      isLoggedIn: state => !!state.userToken,
    },
  });
};

export default createStore;